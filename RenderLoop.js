define([], function() {

  //quicky backfill (cf. paul irish, http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/)
  var requestAnimationFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            function(callback){
              window.setTimeout(callback, 1000 / 60);
            };
  })();


  function RenderLoop(render) {

    this._hitches = [];
    this._jobs = [];
    this._jobsTemp = [];
    this._workQueue = [];
    this._nudged = false;

    var waiting = false;
    var self = this;

    function onFrame(timestep) {
      waiting = false;
      self._processEvents(timestep);
      self._processJobs(timestep);
      if (self._nudged) {//no need to rerender if not nudged.
        self._nudged = false;
        render(timestep);
      }
      self._doWork(timestep);
    }

    this._invalidate = function() {
      if (waiting) {
        return;
      }
      waiting = true;
      requestAnimationFrame(onFrame, document.body);
    };

  }

  RenderLoop.prototype = {
    constructor: RenderLoop,

    hitchEventCallbackBeforeRender: function(callback) {
      var self = this;
      return function(eventArgument) {
        self._hitches.push(callback, eventArgument);
        self._invalidate();
      };
    },
    scheduleBeforeRender: function(callback) {
      this._jobs.unshift(callback);
      this._invalidate();
    },
    throttleWorkAfterRender: function(callback) {
      this._workQueue.unshift(callback);
      this._invalidate();
    },
    _processEvents: function() {
      for (var i = 0; i < this._hitches.length; i += 2) {
        this._hitches[i](this._hitches[i + 1]);//callback(argument).
      }
      this._hitches.length = 0;
    },
    _processJobs: function(time) {

      var tmp = this._jobs;
      this._jobs = this._jobsTemp;

      while (tmp.length) {
        (tmp.pop())(time);
      }
      this._jobsTemp = tmp;

    },

    _doWork: function(timestep) {

      if (!this._workQueue.length) {
        return;
      }

      var target = timestep + 16;//60fps
      do {//force at least once
        (this._workQueue.pop())();
      } while (Date.now() < target && this._workQueue.length);

      if (this._workQueue.length) {
        this._invalidate();
      }
    },
    nudge: function() {
      this._nudged = true;
      this._invalidate();
    }
  };

  return RenderLoop;

});