#RenderLoop

AMD Module for a Javascript render loop to support visualizaiton software. Spin a main render function at 60fps and schedule additional
work before and after render. The additional work will be throttled in order to keep a steady framerate.

## How to:

RenderLoop just executes a render function. Additional work can be scheduled before or after this render function.

### Setup the loop

To setup the loop, just create a new instance with the render function as main

    var renderLoop = new RenderLoop(function(timestep){
       //do some rendering here, e.g. clearing a <canvas> and drawing to it.
    });

### Nudge the loop

The renderloop only draws one frame at the time. To trigger a frame, you need to nudge it.

    var renderLoop = new RenderLoop(function(timestep){
       //do some rendering here, e.g. clearing a <canvas> and drawing to it.
    });
    renderLoop.nudge();

### Schedule updates

Schedule updates to be performed before the render function.

    renderLoop.scheduleBeforeRender(function(timestep){
        //perform an update, for example, an update of an animation
    });

### Throttle work

RenderLoop.js will target 60fps. If the updates and render functions took less than 16ms,
additional work can be done after rendering has completed. The amount of callbacks that are invoked per frame depends on
the amount of time that's left.

To avoid starvation issues, at least one of these post-render callbacks is performed.

    renderLoop.throttleWorkAfterRender(function work(timestep){

        ... do some processing ...

        //it's safe to push on even more work.
        //Execution of work will be interleaved, post-render.
        if (...more work needs to be done...){
            renderLoop.throttleWorkAfterRender(work);
        }

    });
